import Vue from 'vue'
import TopMenu from '~/components/SiteComponents/TopMenu.vue'

import Page from '~/components/Page.vue'
import Section from '~/components/Section.vue'
import Titleheader from '~/components/Titleheader.vue'
import Textarea from '~/components/Textarea.vue'
import Herobanner from '~/components/Herobanner.vue'
import Pagebanner from '~/components/Pagebanner.vue'

import Teaser from '~/components/Teaser.vue'
import Grid from '~/components/Grid.vue'
import Card from '~/components/Card.vue'
import Feature from '~/components/Feature.vue'


Vue.component('design-topmenu', TopMenu)

Vue.component('blok-page', Page)
Vue.component('blok-section', Section)
Vue.component('blok-titleheader', Titleheader)
Vue.component('blok-textarea', Textarea)
Vue.component('blok-herobanner', Herobanner)
Vue.component('blok-pagebanner', Pagebanner)

Vue.component('blok-grid', Grid)
Vue.component('blok-card', Card)
