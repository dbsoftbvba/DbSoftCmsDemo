import Vuex from 'vuex'

export const state = () => ({
  siteStories: []
})

export const mutations = {
  setStories(state, stories) {
    state.siteStories = stories
  }
}

export const actions = {
  async nuxtServerInit(vuexContext, context) {
    return context.app.$storyapi
      .get(`cdn/stories`, {
        "starts_with": "menu/"})
      .then((res) => {
        vuexContext.commit('setStories', res.data.stories)
        return;
      })
      .catch((e) => {
        context.error()
      })
  }
}
