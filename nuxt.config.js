const axios = require('axios')
const env = require('dotenv').config()
module.exports = {
  mode: 'universal',
  env: env.parsed,
  srcDir: 'app',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: { color: '#ff0000' },
  css: ['@/assets/scss/main.scss'],
  plugins: [
    '~/plugins/components',
    '~/plugins/filters',
    { src: '~/plugins/vue-lazyload', ssr: false }
  ],
  modules: [
    '@nuxtjs/axios',
    //  '@nuxtjs/pwa',
    [
      'storyblok-nuxt',
      { accessToken: process.env.NODE_ENV === 'production' ? process.env.STORYBLOK_API_KEY_PUBLIC : process.env.STORYBLOK_API_KEY, cacheProvider: 'memory' }
    ],
    ['bootstrap-vue/nuxt'],
    '@nuxtjs/markdownit'
    //  ['nuxt-fire'],
  ],
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    injected: true
  },
  axios: {},
  router: {
    //  middleware: 'setCacheVersion'
  },
  //buildDir: 'nuxt',
  build: {
    extractCSS: true,
    babel: {
      presets: ({ isServer }) => [
        [
          '@nuxt/babel-preset-app',
          {
            targets: isServer ? { node: '12' } : { browsers: ['defaults'] }
          }
        ]
      ]
    },
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    routes: function() {
      return axios
        .get(
          'https://api.storyblok.com/v1/cdn/stories?version=publised&token=' +
            process.env.STORYBLOK_API_KEY_PUBLIC +
            '&cv=' +
            Math.floor(Date.now() / 1e3)
        )
        .then((res) => {
          const sluglinks = res.data.stories.map((links) => links.full_slug)
          return ['/', ...sluglinks]
        })
        .catch((e) => {
          context.error()
        })
    }
  }
}
